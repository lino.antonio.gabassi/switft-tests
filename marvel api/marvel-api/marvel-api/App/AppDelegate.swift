//
//  AppDelegate.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 26/11/2021.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//         Setup
        setupView()
        FirebaseApp.configure()

        return true
    }
    
    // MARK: - Private methods
    
    private func setupView() {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "HomeViewController", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "home") as! HomeViewController
        
        window?.rootViewController = view
        window?.makeKeyAndVisible()
    }

}
