//
//  Constants.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 19/01/2022.
//

import Foundation
import UIKit


struct Constants {
    
    struct URL {
         
        // MARK: Base URL
        static let BASE_PROTOCOL: String = "https"
        
        static let BASE_DOMAIN: String = "gateway.marvel.com:443/v1/public/"
        
       // MARK: Default query
        static let apiKey: String = "4f845aa435359dd48ca9930acbe8faa7"
        
        static let hash: String = "b735ae75d6704da1f61626de604417e4"
        
        static let ts: String = "1"
    
    }
    
    struct Codes {
    
        // MARK: OK response
        static let responseOk = 200...299
    
    }
    
    struct Colors {
        #if prod
        // MARK: Main color
        static let MAIN_COLOR = UIColor(red: 1.0, green: 0.6542, blue: 0, alpha: 1)
        
        // MARK: Gray background color
        static let BACK_COLOR = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.8)
        
        // MARK: Dark gray color
        static let GRAY_COLOR = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        
        #elseif dev
        // MARK: Main color
        static let MAIN_COLOR = UIColor(red: 0.5, green: 0.6542, blue: 0, alpha: 1)
        
        // MARK: Gray background color
        static let BACK_COLOR = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.8)
        
        // MARK: Dark gray color
        static let GRAY_COLOR = UIColor(red: 0.35, green: 0.35, blue: 0.35, alpha: 1)
        
        #endif
        
    }
    
    struct ErrorMsg {
        
        //MARK: invalid email
        static let INVALID_EMAIL: String = "El Email ingresado no es válido."
        
        //MARK: invalid password
        static let INVALID_PASSWORD: String = "La contraseña no es válida. La contraseña debe tener un mínimo de 8 caracteres, letras y números."
        
        //MARK: invalid both
        static let INVALID_BOTH: String = "Debe ingresar un Email válido y una contraseña."
        
    }
    
    struct UserDefaults {
        static let EMAILKEY: String = "emailcredential"
    }
}
