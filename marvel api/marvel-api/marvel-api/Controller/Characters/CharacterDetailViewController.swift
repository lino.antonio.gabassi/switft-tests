//
//  CharacterDetailViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 29/12/2021.
//

import UIKit
import Kingfisher
import SwiftExtensions

class CharacterDetailViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var charImg: UIImageView!
    @IBOutlet weak var comicsTable: UITableView!
    @IBOutlet weak var comicsTableTitle: UILabel!
    private var cellId = "cellId"
    
    var name: String = "Cargando..."
    var data: String = "Cargando..."
    var img: String = ""
    var comics: [CharacterItems?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.nameLabel.text = self.name
            self.dataLabel.text = self.data
            self.charImg.kf.setImage(with: URL(string: self.img))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.navigationItem.title = "CHARACTER_TITLE".localized
            self.nameLabel.text = "LOADING_TEXT".localized
            self.dataLabel.text = "LOADING_TEXT".localized
            self.comicsTableTitle.text = "CHARACTER_APPEARISSONS".localized
            self.nameLabel.textColor = Constants.Colors.MAIN_COLOR
            self.dataLabel.textColor = Constants.Colors.MAIN_COLOR
            self.charImg.tintColor = Constants.Colors.MAIN_COLOR
        }
    }
    
    private func configTable() {
//        comicsTable.register(UINib(nibName: ComicsTableCustomCell.CellConstants.nibName, bundle: nil), forCellReuseIdentifier: ComicsTableCustomCell.CellConstants.reuseIdentifier)
        
        DispatchQueue.main.async {
            self.comicsTable.register(CustomCellSmall.self, forCellReuseIdentifier: self.cellId)
            
            self.comicsTable.tableFooterView = UIView()
            
            self.comicsTable.dataSource = self
        }
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics?.count ?? 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: ComicsTableCustomCell.CellConstants.reuseIdentifier, for: indexPath) as? ComicsTableCustomCell else {return UITableViewCell() }
//        cell.comicName.text = comics?[indexPath.row]?.name
        
        guard let cell = comicsTable.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CustomCellSmall else { return UITableViewCell() }
        
        DispatchQueue.main.async {
            cell.itemNameLabel.text = self.comics?[indexPath.row]?.name
        }
        
        return cell
    }

}
