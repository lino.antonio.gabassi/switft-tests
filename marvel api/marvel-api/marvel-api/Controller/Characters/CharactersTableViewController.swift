//
//  TableViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 20/12/2021.
//

import UIKit
import Kingfisher
import SwiftExtensions

class CharactersTableViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var isLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bottomBarItem: UITabBarItem!
    @IBOutlet weak var CharacterTable: UITableView!
    
    private var results: [CharacterResults?]?
    private var offset: Int = 0
    private var isFetchingNextPage = false
    private var cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCharacters()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.bottomBarItem.title = "CHARACTER_TAB_ITEM".localized
            self.isLoadingIndicator.color = Constants.Colors.MAIN_COLOR
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        offset = 0
        
    }
    
    private func configTable() {
//        CharacterTable.register(UINib(nibName: CustomTableViewCell.CellConstants.nibName, bundle: nil), forCellReuseIdentifier: CustomTableViewCell.CellConstants.reuseIdentifier)
        
        DispatchQueue.main.async {
            self.CharacterTable.register(CustomCellBig.self, forCellReuseIdentifier: self.cellId)
            
            self.CharacterTable.tableFooterView = UIView()
            
            self.CharacterTable.dataSource = self
            
            self.CharacterTable.prefetchDataSource = self
        }
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results?.count ?? 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CustomCellBig else { return UITableViewCell() }
        
        let imgurl = (results?[indexPath.row]?.thumbnail?.path)! + "." + (results?[indexPath.row]?.thumbnail?._extension ?? "jpg")
        
        DispatchQueue.main.async {
            cell.itemNameLabel.text = self.results?[indexPath.row]?.name
            cell.itemImage.kf.setImage(with: URL(string: imgurl))
        }
        
        cell.buttonPressed = {
            let vc = UIStoryboard.init(name: "CharactersScreen", bundle: Bundle.main).instantiateViewController(withIdentifier: "character") as? CharacterDetailViewController
            vc?.name = (self.results?[indexPath.row]?.name)!
            if self.results?[indexPath.row]?.description != "" {
                vc?.data = (self.results?[indexPath.row]?.description)!
            } else {
                vc?.data = NSLocalizedString("NO_DATA_TEXT", comment: "")
            }
            vc?.img = (self.results?[indexPath.row]?.thumbnail?.path)! + "." + (self.results?[indexPath.row]?.thumbnail?._extension ?? "jpg")
            vc?.comics = self.results?[indexPath.row]?.comics?.items
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        return cell
    }
    
    private func getCharacters() {
        isLoadingIndicator.startAnimating()
        isFetchingNextPage = true
        GetCharacters.shared.getCharacters(offset: self.offset) {
            (results) in
            self.results = results
            self.CharacterTable.reloadData()
            self.isFetchingNextPage = false
            self.isLoadingIndicator.stopAnimating()
        }
    }
    
    private func getMoreCharacters() {
        guard !isFetchingNextPage else { return }
        isLoadingIndicator.startAnimating()
        self.offset += 15
        GetCharacters.shared.getCharacters(offset: self.offset) {
            (results) in
            self.results?.append(contentsOf: results)
            self.CharacterTable.reloadData()
            self.isLoadingIndicator.stopAnimating()
        }
    }
    
}

extension CharactersTableViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let needsFetch = indexPaths.contains { $0.row >= self.results!.count - 3}
        if needsFetch {
            getMoreCharacters()
        }
    }
    
}
