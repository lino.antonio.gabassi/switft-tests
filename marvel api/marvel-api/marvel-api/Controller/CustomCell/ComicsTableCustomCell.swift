//
//  ComicsTableCustomCell.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 03/01/2022.
//

//import UIKit
//
//class ComicsTableCustomCell: UITableViewCell {
//
//    enum CellConstants {
//        static let nibName = String(describing: ComicsTableCustomCell.self)
//        static let reuseIdentifier = String(describing: ComicsTableCustomCell.self)
//    }
//
//    @IBOutlet weak var comicName: UILabel!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        comicName.textColor = Constants.Colors.MAIN_COLOR
//
//    }
//
//}
