//
//  CustomCellBig.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 17/02/2022.
//


import UIKit
import SwiftExtensions

class CustomCellBig: UITableViewCell {
    
    var buttonPressed : (() -> ()) = {}
    
    public let itemNameLabel : UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
        nameLabel.textAlignment = .left
        nameLabel.textColor = Constants.Colors.MAIN_COLOR
        nameLabel.numberOfLines = 0
        return nameLabel
    }()
    
    public let itemImage : UIImageView = {
        let imgView = UIImageView(image: UIImage(systemName: "person.fill"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        imgView.tintColor = Constants.Colors.MAIN_COLOR
        return imgView
    }()
    
    public let enterDetail : UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(systemName: "chevron.right.circle.fill"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        btn.tintColor = Constants.Colors.MAIN_COLOR
        return btn
    }()
    
    @objc func enterDetailAction() {
        buttonPressed()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
        DispatchQueue.main.async {
            self.contentView.addSubview(self.itemNameLabel)
            self.contentView.addSubview(self.itemImage)
            self.contentView.addSubview(self.enterDetail)
            
            self.contentView.backgroundColor = Constants.Colors.BACK_COLOR
            self.contentView.makeCornerRadius()
            
            self.itemImage.anchor(top: self.contentView.topAnchor, left: self.contentView.leftAnchor, bottom: self.contentView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 100, height: 100, enableInsets: false)
            self.itemNameLabel.anchor(top: self.contentView.topAnchor, left: self.itemImage.rightAnchor, bottom: self.contentView.bottomAnchor, right: self.enterDetail.leftAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: 0, height: 0, enableInsets: false)
            self.enterDetail.anchor(top: self.contentView.topAnchor, left: nil, bottom: self.contentView.bottomAnchor, right: self.contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 10, width: 20, height: 0, enableInsets: false)
            self.enterDetail.addTarget(self, action: #selector(self.enterDetailAction), for: .touchUpInside)
        }
    }
    
    override func layoutSubviews() {
           super.layoutSubviews()
   
           contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
       }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
