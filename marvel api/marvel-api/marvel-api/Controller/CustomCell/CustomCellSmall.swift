//
//  CustomCellSmall.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 18/02/2022.
//

import UIKit
import SwiftExtensions

class CustomCellSmall: UITableViewCell {
    public let itemNameLabel : UILabel = {
        let nameLabel = UILabel()
        nameLabel.font = UIFont.boldSystemFont(ofSize: 19)
        nameLabel.textAlignment = .left
        nameLabel.textColor = Constants.Colors.MAIN_COLOR
        nameLabel.numberOfLines = 0
        return nameLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
        DispatchQueue.main.async {
            self.contentView.addSubview(self.itemNameLabel)
            
            self.contentView.makeCornerRadius()
            
            self.itemNameLabel.anchor(top: self.contentView.topAnchor, left: self.leftAnchor, bottom: self.contentView.bottomAnchor, right: self.rightAnchor, paddingTop: 20, paddingLeft: 10, paddingBottom: 20, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
