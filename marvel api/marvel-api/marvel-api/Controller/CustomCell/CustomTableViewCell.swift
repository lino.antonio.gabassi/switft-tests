//
//  CustomTableViewCell.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 27/12/2021.
//
//
//import UIKit
//import SwiftExtensions
//
//class CustomTableViewCell: UITableViewCell {
//
//    enum CellConstants {
//        static let nibName = String(describing: CustomTableViewCell.self)
//        static let reuseIdentifier = String(describing: CustomTableViewCell.self)
//    }
//
//    var buttonPressed : (() -> ()) = {}
//
//    @IBOutlet weak var label: UILabel!
//    @IBOutlet weak var thumbnail: UIImageView!
//    @IBOutlet weak var enterDetail: UIButton!
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        contentView.make_cornerRadius()
//        label.textColor = Constants.Colors.MAIN_COLOR
//        enterDetail.tintColor = Constants.Colors.MAIN_COLOR
//        contentView.backgroundColor = Constants.Colors.BACK_COLOR
//    }
//
//    @IBAction func enterDetailAction(_ sender: UIButton) {
//        buttonPressed()
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
//    }
//}

