//
//  EventDetailViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 05/01/2022.
//

import UIKit
import SwiftExtensions

class EventDetailViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var comicsTable: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    private var cellId = "cellId"
    
    var comics: [EventItems?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.titleLabel.text = "COMIC_LIST_TITLE".localized
            self.titleLabel.textColor = Constants.Colors.MAIN_COLOR
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        comicsTable.register(UINib(nibName: ComicsTableCustomCell.CellConstants.nibName, bundle: nil), forCellReuseIdentifier: ComicsTableCustomCell.CellConstants.reuseIdentifier)
        
        DispatchQueue.main.async {
            self.comicsTable.register(CustomCellSmall.self, forCellReuseIdentifier: self.cellId)
            
            self.comicsTable.tableFooterView = UIView()
            
            self.comicsTable.dataSource = self
            
        }
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics?.count ?? 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: ComicsTableCustomCell.CellConstants.reuseIdentifier, for: indexPath) as? ComicsTableCustomCell else {return UITableViewCell() }
//
//        cell.comicName.text = comics?[indexPath.row]?.name
        guard let cell = comicsTable.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CustomCellSmall else { return UITableViewCell() }
        
        DispatchQueue.main.async {
            cell.itemNameLabel.text = self.comics?[indexPath.row]?.name
        }
        
        return cell
    }

}
