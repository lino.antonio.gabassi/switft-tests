//
//  TableViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 20/12/2021.
//

import UIKit
import Kingfisher
import SwiftExtensions

class EventsTableViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var isLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var bottomBarItem: UITabBarItem!
    @IBOutlet weak var EventsTable: UITableView!
    private var cellId = "cellId"
    
    var results: [EventResults?]?
    private let offset: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.bottomBarItem.title = "EVENTS_TAB_ITEM".localized
            self.isLoadingIndicator.color = Constants.Colors.MAIN_COLOR
        }
    }
    
    private func configTable() {
//        EventsTable.register(UINib(nibName: CustomTableViewCell.CellConstants.nibName, bundle: nil), forCellReuseIdentifier: CustomTableViewCell.CellConstants.reuseIdentifier)
        
        DispatchQueue.main.async {
            self.EventsTable.register(CustomCellBig.self, forCellReuseIdentifier: self.cellId)
            
            self.EventsTable.tableFooterView = UIView()
            
            self.EventsTable.dataSource = self
        }
    }

    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results?.count ?? 0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? CustomCellBig else { return UITableViewCell() }
        
        DispatchQueue.main.async {
            cell.itemNameLabel.text = self.results?[indexPath.row]?.title

            let imgurl = (self.results?[indexPath.row]?.thumbnail?.path)! + "." + (self.results?[indexPath.row]?.thumbnail?._extension ?? "jpg")

            cell.itemImage.kf.setImage(with: URL(string: imgurl))
        }
        
        cell.buttonPressed = {
            let vc = UIStoryboard.init(name: "EventsScreen", bundle: Bundle.main).instantiateViewController(withIdentifier: "event") as? EventDetailViewController
            
            vc?.comics = self.results?[indexPath.row]?.comics?.items
            
            self.navigationController?.present(vc!, animated: true)
        }
        
        return cell
    }
    
    private func getEvents() {
        isLoadingIndicator.startAnimating()
        GetEvents.shared.getEvents(offset: self.offset) {
            (results) in
            self.results = results
            self.EventsTable.reloadData()
            self.isLoadingIndicator.stopAnimating()
        }
    }
    
}
