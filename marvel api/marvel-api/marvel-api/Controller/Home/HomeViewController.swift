//
//  HomeViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 26/11/2021.
//

import UIKit
import FirebaseAuth
import SwiftExtensions

protocol LogOutProtocol {
    func logOut()
}

final class HomeViewController: UIViewController, LogOutProtocol {
    func logOut() {
        do {
            try Auth.auth().signOut()
            navigationController?.popViewController(animated: true)
            UserDefaults.standard.set(false, forKey: "Loged")
        } catch {
            print("Error al desloguear")
        }
    }
    
    @IBOutlet weak var mainPageLabel: UILabel!
    @IBOutlet weak var logOutButton: UIButton?
    @IBOutlet weak var emailLabel: UILabel!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.mainPageLabel.text = "MAIN_PAGE_LABEL".localized
            self.logOutButton?.setTitle("LOGOUT_BUTTON".localized, for: .normal)
            self.logOutButton?.setTitleColor(Constants.Colors.MAIN_COLOR, for: .normal)
            guard let email = UserDefaults.standard.string(forKey: Constants.UserDefaults.EMAILKEY) else { return }
            self.emailLabel.text = "Email: " + email
        }
    }

    @IBAction func logOutButtonAction(_ sender: Any) {
        logOut()
    }
    
}
