//
//  ViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 22/12/2021.
//

import UIKit
import Firebase
import SwiftExtensions

protocol AuthProtocol {
    func logIn()
    func signUp()
}

class LoginViewController: UIViewController, AuthProtocol {
    func signUp() {
        if isValidEmail(emailTextField.text!), isValidPassword(passwordTextField.text!) {
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { authResult, error in
                if authResult != nil {
                    let storyboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let view = storyboard.instantiateViewController(withIdentifier: "home2")
                    self.navigationController?.pushViewController(view, animated: true)
                    UserDefaults.standard.set(true, forKey: "Loged")
                    UserDefaults.standard.set(self.emailTextField.text, forKey: Constants.UserDefaults.EMAILKEY)
                } else {
                    self.showDefaultAlert(error!.localizedDescription)
                }
            }
        } else {
            self.showDefaultAlert("ERROR_INVALID_CREDENTIAL".localized)
        }
    }
    
    func logIn() {
        if isValidEmail(emailTextField.text!), isValidPassword(passwordTextField.text!) {
            Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { [weak self] authResult, error in
              guard let _ = self else { return }
                if authResult != nil {
                    let storyboard = UIStoryboard(name: "HomeViewController", bundle: nil)
                    let view = storyboard.instantiateViewController(withIdentifier: "home2")
                    self?.navigationController?.pushViewController(view, animated: true)
                    UserDefaults.standard.set(true, forKey: "Loged")
                    UserDefaults.standard.set(self?.emailTextField.text, forKey: Constants.UserDefaults.EMAILKEY)
                } else {
                    self!.showDefaultAlert(error!.localizedDescription)
                }
            }
        } else {
            self.showDefaultAlert("ERROR_INVALID_CREDENTIAL".localized)
        }
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var showPasswordButton: UIButton!
    
    private var vc = HomeViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "Loged") {
            let storyboard = UIStoryboard(name: "HomeViewController", bundle: nil)
            let view = storyboard.instantiateViewController(withIdentifier: "home2")
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.title = "LOGIN_TITLE".localized
            
            UIView.animate(withDuration: 1, delay: 1) {
                self.logInButton.alpha = 1
                self.signUpButton.alpha = 1
            }
            
            UIView.animate(withDuration: 1, delay: 2) {
                self.logInButton.backgroundColor = Constants.Colors.GRAY_COLOR
                self.signUpButton.backgroundColor = Constants.Colors.MAIN_COLOR
            }
            
            self.emailTextField.placeholder = "EMAIL_FIELD_PLACEHOLDER".localized
            self.passwordTextField.placeholder = "PASS_FIELD_PLACEHOLDER".localized
            self.logInButton.setTitle("LOGIN_BUTTON".localized, for: .normal)
            self.signUpButton.setTitle("SIGNUP_BUTTON".localized, for: .normal)
            self.showPasswordButton.tintColor = Constants.Colors.MAIN_COLOR
        }
        
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        signUp()
    }

    @IBAction func logInButtonAction(_ sender: Any) {
        logIn()
    }
    
    @IBAction func showPasswordAction(_ sender: Any) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        if passwordTextField.isSecureTextEntry {
            showPasswordButton.setImage(UIImage(systemName: "eye"), for:.normal)
        } else { showPasswordButton.setImage(UIImage(systemName: "eye.fill"), for:.normal)
        }
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    private func isValidPassword(_ password: String) -> Bool {
        let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"

        let passPred = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passPred.evaluate(with: password)
    }
    
    private func showDefaultAlert(_ message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
