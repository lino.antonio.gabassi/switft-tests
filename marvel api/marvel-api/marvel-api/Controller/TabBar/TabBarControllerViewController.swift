//
//  TabBarControllerViewController.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 27/12/2021.
//

import UIKit
import SwiftExtensions

final class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.navigationItem.setHidesBackButton(true, animated: true)
            self.navigationItem.title = "HOME_TITLE".localized
        }
    }

}
