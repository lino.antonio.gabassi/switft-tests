//
//  CharacterData.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 27/12/2021.
//

import Foundation

struct CharacterResponse: Decodable {
    let code: Int?
    let data: CharacterData?
}

struct CharacterData: Decodable {
    let offset: Int?
    let limit: Int?
    let total: Int?
    let count: Int?
    let results: [CharacterResults?]
}

struct CharacterResults: Decodable {
    let id: Int?
    let name: String?
    let description: String?
    let thumbnail: CharacterThumbnail?
    let comics: CharacterComics?
}

struct CharacterThumbnail: Decodable {
    let path: String?
    let _extension: String?
}

struct CharacterComics: Decodable {
    let available: Int?
    let collectionURI: String?
    let items: [CharacterItems?]
}

struct CharacterItems: Decodable {
    let name: String?
}
