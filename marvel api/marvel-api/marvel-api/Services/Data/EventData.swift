//
//  EventData.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 29/12/2021.
//

import Foundation

struct EventResponse: Decodable {
    let code: Int?
    let data: EventData?
}

struct EventData: Decodable {
    let offset: Int?
    let limit: Int?
    let total: Int?
    let count: Int?
    let results: [EventResults?]
}

struct EventResults: Decodable {
    let id: Int?
    let title: String?
    let thumbnail: EventThumbnail?
    let comics: EventComics?
}

struct EventThumbnail: Decodable {
    let path: String?
    let _extension: String?
}

struct EventComics: Decodable {
    let available: Int?
    let collectionURI: String?
    let items: [EventItems?]
}

struct EventItems: Decodable {
    let name: String?
}
