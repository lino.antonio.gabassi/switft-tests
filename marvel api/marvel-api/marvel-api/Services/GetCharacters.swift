//
//  GetCharacters.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 27/12/2021.
//

import Foundation
import Alamofire

final class GetCharacters {
    
    static let shared = GetCharacters()
    
    var results: [CharacterResults?]?
    
    var limit: String = ""
    
    func getCharacters(offset: Int, completion: @escaping ([CharacterResults?]) -> Void ){
        #if prod
        limit = "15"
        
        #elseif dev
        limit = "25"
        
        #endif
        
        let URL = "\(Constants.URL.BASE_PROTOCOL)://\(Constants.URL.BASE_DOMAIN)characters?limit=\(limit)&offset=\(offset)&apikey=\(Constants.URL.apiKey)&hash=\(Constants.URL.hash)&ts=\(Constants.URL.ts)"
        
        AF.request(URL, method: .get).validate(statusCode: Constants.Codes.responseOk).responseDecodable (of: CharacterResponse.self) {
            response in
            if let character = response.value?.data?.results {
                completion(character)
            } else {
                print(response.error?.responseCode ?? "No error")
                
            }
        }
    }
    
}

// AL PRINCIPIO SE USÓ EL SERVICIO NATIVO DE IOS, PERO SE CAMBIÓ A ALAMOFIRE POR REQUERIMIENTOS DEL CHALLENGE
    
    
    
//    static let shared = GetCharacters()
//    var res: Response?
//    var results: [Results?]?
//    let baseURL = "https://gateway.marvel.com:443/v1/public/"
//    let decoder = JSONDecoder()
//    func getCharacter(offset: String) -> [Results?]? {
//            let url = URL(string: BaseURL+"characters?limit=15&offset="+offset+"&apikey=4f845aa435359dd48ca9930acbe8faa7&hash=b735ae75d6704da1f61626de604417e4&ts=1")!
//
//        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
//            guard let data = data else { return }
//            do {
//                self.res = try self.decoder.decode(Response.self, from: data)
//                if let results = self.res?.data?.results{
//                        self.results = results
//                } else {
//                    print("If Let")
//                }
//            } catch let e{
//                print("Error: ", e)
//            }
//        }
//        task.resume()
//        return self.results
//        }
