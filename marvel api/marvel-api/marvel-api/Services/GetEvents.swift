//
//  GetEvents.swift
//  marvel-api
//
//  Created by Nicolas Daniel Laugas on 29/12/2021.
//

import Foundation
import Alamofire

final class GetEvents {
    
    static let shared = GetEvents()
    
    var results: [EventResults?]?
    
    private let apiKey = "4f845aa435359dd48ca9930acbe8faa7"
    private let hash = "b735ae75d6704da1f61626de604417e4"
    private let ts = "1"
    private let baseUrl = "https://gateway.marvel.com:443/v1/public/"
    private let responseOk = 200...299
    
    
    func getEvents(offset: Int, completion: @escaping ([EventResults?]) -> Void ){
        
        let url = "\(baseUrl)events?orderBy=startDate&limit=25&offset=\(offset)&apikey=\(apiKey)&hash=\(hash)&ts=\(ts)"
        
        AF.request(url, method: .get).validate(statusCode: responseOk).responseDecodable (of: EventResponse.self) { response in
            
            if let event = response.value?.data?.results {
                print(event)
                completion(event)
            } else {
                print(response.error?.responseCode ?? "No error")
                
            }
        }
    }
    
    func getEventById(id: Int, completion: @escaping ([EventResults?]) -> Void ){
        
        let URL = "\(Constants.URL.BASE_PROTOCOL)://\(Constants.URL.BASE_DOMAIN)events/\(id)?&apikey=\(Constants.URL.apiKey)&hash\(Constants.URL.hash)&ts=\(Constants.URL.ts)"
        
        AF.request(URL, method: .get).validate(statusCode: Constants.Codes.responseOk).responseDecodable (of: EventResponse.self) { response in
            
            if let event = response.value?.data?.results {
                completion(event)
            } else {
                print(response.error?.responseCode ?? "No error")
                
            }
        }
    }
    
}
